* [ ] How are classes different than functions?

* [ ] Please name three different ways we organize code in Python and explain what the benefit of each is?

* [ ] Given these classes:

class A:
    def __init__(self, a):
        self.a = A
    def speak(self):
        print(self.a)

class B:
    def __init__(self, b):
        self.b = B
    def speak(self):
        print(self.b)

class Child(A,B):
    def __init__(self, a, b):
        self.a = a
        self.b = b

child = Child("item1", "item2")
child.speak()

what will be the output? 
"item1"
Please describe how the inheritance order plays a role in determining the output.
"Follow along with Thonny to describe how it occurs"

* [ ] Please explain when you would choose to use a view function and when you would choose to use a class-based view.
