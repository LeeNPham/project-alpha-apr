Legend * [ ] * [x]

Scoring
The following table shows how your final grade will be calculated.

Source of points	Points possible
Quality of code	30
Feature unit tests	144

Quality of code
The quality of your code will be determined by the code cleanliness of your Python and HTML templates. We will visually inspect the names of variables, path names, methods, functions, and classes that you write to determine if they are named in a way that another software engineer can determine the meaning of the name with respect to its value.

The starting repository for this application contains a .vscode directory that will automatically format your Python for you using a tool named black when you save it. It will not fix some errors, like "unused imports".

We will run the following utilities to determine the cleanliness of your code. You should run them, too, before submitting to make sure that you get all the points possible.

* [x] flake8 accounts projects tasks
* [x] black --check accounts projects tasks
* [x] djhtml -i «path to HTML template»
* [x] Placed three lines at the end of Feature 19 planning doc


Feature	Points	What is done	                             Depends on
1	      5	      Install dependencies	
2	      7	      Set up the Django project and apps	       1
3	      12	    The Project model	                         2
4	      1	      Registering Project in the admin	         3
5	      9	      The Project list view	                     3
6	      2	      Default path redirect	                     5
7	      10	    Login page	                               2
8	      3	      Require login for Project list view	       5, 7
9	      4	      Logout page	                               7
10	    10	    Sign up page	                             2
11	    20	    The Task model	                           3
12	    1	      Registering Task in the admin	             11
13	    15	    The Project detail view	                   12
14	    11	    The Project create view                    2
15	    13	    The Task create view	                     11
16	    7	      Show "My Tasks" list view	                 11
17	    11	    Completing a task	                         16
18	    1	      Markdownify	                               17
19	    2	      Navigation	                               5, 7, 9, 10, 16

References
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/66-assessment-project.md

Helpful commands
This section is here to give you a reference for common commands that you may need to use during this (and other) projects.

# Create a new virtual environment
# Make sure you do this in a new terminal window or
# that you have NOT ALREADY activated a virtual
# environment
python -m venv .venv

# Activate a virtual environment
source ./.venv/bin/activate  # macOS
./.venv/Scripts/Activate.ps1 # Windows

# Deactivating your virtual environment
# If you are in an active virtual environment,
# then you can deactivate it with this
# command
deactivate

# Update pip
python -m pip install --upgrade pip

# Install dependencies from requirements.txt
pip install -r requirements.txt

# Create a brand new requirements.txt file from 
# the installed pip packages
pip freeze > requirements.txt

# Create a new Django project in the current
# directory, normally the directory that contains
# your .venv directory. DO NOT FORGET THE DOT!
django-admin startproject «name» .

# Create a new Django app
python manage.py startapp «name»

# Run your development server
python manage.py runserver

# Test your application
python manage.py test

# Make migrations after creating or changing
# a model class
python manage.py makemigrations

# Apply the migrations to your database after
# making them
python manage.py migrate


QuerySets
These are the primary methods for getting objects from the database

# Get all the objects
model_instance_list = ModelName.objects.all()

# Get the first object
model_instance = ModelName.objects.first()

# Get a single object by one of it's attributes like:
# By primary key
model_instance = ModelName.objects.get(pk=1)

# By some other field
model_instance = ModelName.objects.get(some_field="Some field value")

# Filter is like get, but it gets all the objects that match
model_instance_list = ModelName.objects.filter(some_field="Some field value")



Feature 1
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/67-assessment-project.md

* [x] Fork and clone the starter project from Project Alpha 
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
* [x] Upgrade pip
* [x] Install django
* [x] Install black
* [x] Install flake8
* [x] Install djhtml
* [x] Deactivate your virtual environment
* [x] Activate your virtual environment
* [x] Use pip freeze to generate a requirements.txt file
* [x] Run python manage.py test tests.test_feature_01 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 2
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/68-assessment-project.md

* [x] Create a Django project named tracker so that the manage.py file is in the top directory
* [x] Create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list
* [x] Create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list
* [x] Create a Django app named tasks and install it in the tracker Django project in the INSTALLED_APPS list
* [x] Run the migrations
* [x] Create a super user
* [x] Run python manage.py test tests.test_feature_02 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 3
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/69-assessment-project.md

* [x] Create a Project model in the projects Django app.
* [x] The Project model should have the following attributes.
    * [x] name = string with max length 200
    * [x]] description = string no max length
    * [x] members = many to many field refers to auth.user model, related name "projects"
* [x] Project model should implicitly convert to a string with the __str__ method that is the value of the name property.
* [x] Run the migrations
* [x] Create a super user
* [x] Run python manage.py test tests.test_feature_03 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 4
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/70-assessment-project.md

* [x] Register the Project model with the admin so that you can see it in the Django admin site.
* [x] Run python manage.py test tests.test_feature_04 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 5
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/71-assessment-project.md

* [x] Create a view that will get all of the instances of the Project model and puts them in the context for the template
* [x] Register that view in the projects app for the path "" and the name "list_projects" in a new file named projects/urls.py
* [x] Include the URL patterns from the projects app in the tracker project with the prefix "projects/"
* [x] Create a template for the list view that complies with the following specifications
* [x] the fundamental five in it
  * [x] a main tag that contains
    * [x] div tag that contains
      * [x] an h1 tag with the content "My Projects"
      * [x] if there are no projects created, then
        * [x] a p tag with the content "You are not assigned to any projects"
      * [x] otherwise
        * [x] a table that has two columns
          * [x] The first with the header "Name" and the rows with the names of the projects
          * [x] The second with the header "Number of tasks" and nothing in those rows because we don't yet have tasks
* [x] Run python manage.py test tests.test_feature_05 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 6
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/72-assessment-project.md

* [x] In the tracker urls.py, use the RedirectView to redirect from "" to the name of path for the list view that you created in the previous feature. Register that path a name of "home".
* [x] Utilize reverselazy to bring back to main page and reference "" url request as "home"
* [x] Run python manage.py test tests.test_feature_06 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 7
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/73-assessment-project.md

* [x] Register the LoginView  in your accounts urls.py with the path "login/" and the name "login"
* [x] Include the URL patterns from the accounts app in the tracker project with the prefix "accounts/"
* [x] Create a templates directory under accounts
* [x] Create a registration directory under templates
* [x] Create an HTML template named login.html in the registration directory
  * [x] the fundamental five in it
  * [x] a main tag that contains
    * [x] div tag that contains
      * [x] an h1 tag with the content "Login"
      * [x] a form tag with method "post" that contains any kind of HTML structures but must include
        * [x] an input tag with type "text" and name "username"
        * [x] an input tag with type "password" and name "password"
        * [x] a button with the content "Login"
* [x] Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
* [x] In the tracker settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home" which will redirect us to the path (not yet created) with the name "home"
* [x] Run python manage.py test tests.test_feature_07 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 8
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/74-assessment-project.md

* [x] Protect the list view for the Project model so that only a person that has logged in can access it
* [x] Change the queryset of the view to filter the Project objects where members equals the logged in user
* [X] Use the queryset that matches the logged in user with the projects list with request_user.all vs projects
* [x] Run python manage.py test tests.test_feature_08 
* [x] and passed
* [x] Add, commit, and pushed changes


Feature 9
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/75-assessment-project.md

* [x] In the accounts/urls.py file,
* [x] Import the LogoutView from the same module that you imported the LoginView from
* [x] Register that view in your urlpatterns list with the path "logout/" and the name "logout"
* [x] In the tracker settings.py file, create and set the variable LOGOUT_REDIRECT_URL to the value "login" which will redirect the logout view to the login page
* [x] Ran python manage.py test tests.test_feature_09 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 10
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/76-assessment-project.md

* [x] You'll need to import the UserCreationForm from the built-in auth forms 
* [x] You'll need to use the special create_user  method to create a new user account from their username and password
* [x] You'll need to use the login  function that logs an account in
* [x] After you have created the user, redirect the browser to the path registered with the name "home"
* [x] Create an HTML template named signup.html in the registration directory
  * [x] the fundamental five in it
  * [x] a main tag that contains
    * [x] div tag that contains
      * [x] an h1 tag with the content "Signup"
      * [x] a form tag with method "post" that contains any kind of HTML structures but must include
        * [x] an input tag with type "text" and name "username"
        * [x] an input tag with type "password" and name "password1"
        * [x] an input tag with type "password" and name "password2"
        * [x] a button with the content "Signup"
* [x] Put a post form in the signup.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
* [x] Run python manage.py test tests.test_feature_10 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 11
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/77-assessment-project.md

* [x] Create a Task model in the tasks Django app.
* [x] Include name as string with max length 200 characters
* [x] Include start_date with Date-Time
* [x] Include due_date with Date-Time
* [x] Include is_completed with Bool
* [x] Include project as foreignkey with related name "tasks" and on_delete = models.CASCADE
* [x] Include assignee as foreignkey with null=True, related name "tasks" and on_delete = set_null
* [x] Code for __str__ function that returns self.name
* [x] Migrate all changes
* [x] Run python manage.py test tests.test_feature_11 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 12
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/78-assessment-project.md

* [x] Register the Task model with the admin so that you can see it in the Django admin site, in projects/admin
* [x] Run python manage.py test tests.test_feature_12 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 13
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/79-assessment-project.md

* [x] Create a view that shows the details of a particular project
* [x] A user must be logged in to see the view
* [x] In the projects urls.py file, register the view with the path "<int:pk>/" and the name "show_project"
* [x] Create a template to show the details of the project and a table of its tasks
  * [x] the fundamental five in it
  * [x] a main tag that contains
    * [x] div tag that contains
      * [x] an h1 tag with the project's name as its content
      * [x] a p tag with the project's description in it
      * [x] an h2 tag that has the content "Tasks"
      * [x] if the project has tasks, then
        * [x] a table that contains five columns with the headers "Name", "Assignee", "Start date", "Due date", and "Is completed" with rows for each task in the project
      * [x] otherwise
        * [x] a p tag with the content "This project has no tasks"
* [x] Update the list template to show the number of tasks for a project
* [x] Update the list template to have a link from the project name to the detail view for that project
* [x] Create if loop for projects.tasks.all.is_completed to render yes or no instead of true or false
* [x] Recorrect spelling error from This project has no tasks. Accidentally capitalized "P" in "project"
* [x] Run python manage.py test tests.test_feature_13 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 14
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/80-assessment-project.md

* [x] Create a create view for the Project model that will show the name, description, and members properties in the form and handle the form submission to create a new Project
* [x] A person must be logged in to see the view
* [x] If the project is successfully created, it should redirect to the detail page for that project
* [x] Register that view for the path "create/" in the projects urls.py and the name "create_project"
* [x] Create an HTML template that shows the form to create a new Project (see the template specifications below)
  * [x] the fundamental five in it
  * [x] a main tag that contains
    * [x] div tag that contains
      * [x] an h1 tag with the content "Create a new project"
      * [x] a form tag with method "post" that contains any kind of HTML structures but must include
        * [x] an input tag with type "text" and name "name"
        * [x] a textarea tag with the name "description"
        * [x] a select tag with name "members"
        * [x] a button with the content "Create"
* [x] Add a link to the list view for the Project that navigates to the new create view
  * [x] delete foreignkey portion for assignees as it was a manytomanyfield set instead
* [x] Run python manage.py test tests.test_feature_14 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 15
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/81-assessment-project.md

* [x] Create a view that will show a form to create an instance of the Task model for all properties except the is_completed field
* [x] Create in the tasks directory
* [x] The view must only be accessible by people that are logged in
* [x] When the view successfully handles the form submission, have it redirect to the detail page of the task's project
* [x] Register that view in the tasks app for the path "create/" and the name "create_task" in a new file named tasks/urls.py
* [x] Include the URL patterns from the tasks app in the tracker project with the prefix "tasks/"
* [x] Create a template for the create view that complies with the following specifications
  * [x] the fundamental five in it
  * [x] a main tag that contains
    * [x] div tag that contains
      * [x] an h1 tag with the content "Create a new task"
      * [x] a form tag with method "post" that contains any kind of HTML structures but must include
        * [x] an input tag with type "text" and name "name"
        * [x] an input tag with type "text" and name "start_date"
        * [x] an input tag with type "text" and name "due_date"
        * [x] a select tag with name "projects"
        * [x] a select tag with name "assignee"
        * [x] a button tag with the content "Create"
* [x] Add a link to create a task from the project detail page that complies with the following specifications
* [x] Run python manage.py test tests.test_feature_15 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 16
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/82-assessment-project.md

* [x] Create a list view for the Task model with the objects filtered so that the person only sees the tasks assigned to them by filtering with the assignee equal to the currently logged in user
* [x] The view must only be accessible by people that are logged in
* [x] Register that view in the tasks app for the path "mine/" and the name "show_my_tasks" in the tasks urls.py file
* [x] Create an HTML template that conforms with the following specification
  * [x] the fundamental five in it
  * [x] a main tag that contains
    * [x] div tag that contains
      * [x] an h1 tag with the content "My Tasks"
      * [x] if there are tasks assigned to the person
      * [x] a table with the headers "Name", "Start date", "End date", "Is completed" and a row for each task that is assigned to the logged in person
      * [x] use only 4 td opening tags only
      * [x] In the last column, if the task is completed, it should show the word "Done",otherwise, it should show nothing
  * [x] otherwise
      * [x] a p tag with the content "You have no tasks"
* [x] Run python manage.py test tests.test_feature_16 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 17
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/83-assessment-project.md

* [x] Create an update view for the Task model that only is concerned with the is_completed field
* [x] When the view successfully handles a submission, it should redirect to the "show_my_tasks" URL path, that is, it should redirect to the "My Tasks" view (success_url property on a view class)
* [x] Register that view in the tasks app for the path "<int:pk>/complete/" and the name "complete_task" in the tasks urls.py file
* [x] You do not need to make a template for this view
* [x] Modify the "My Tasks" view to comply with the template specification
* [x] Add the template specs into the template for My Tasks
<form method="post" action="{% url 'complete_task' task.id %}">
  {% csrf_token %}
  <input type="hidden" name="is_completed" value="True">
  <button>Complete</button>
</form>
* [x] Run python manage.py test tests.test_feature_17 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 18
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/84-assessment-project.md

* [x] Install the django-markdownify package using pip (the first pip command in the instructions) and put it into the INSTALLED_APPS in the tracker settings.py according to the Installation  instructions block
* [x] Also, in the tracker settings.py file, add the configuration setting to disable sanitation 
* [x] In your the template for the Project detail view, load the markdownify template library as shown in the Usage  section
* [x] Replace the p tag and {{ project.description }} in the Project detail view with this code
* [x] {{ project.description|markdownify }}
* [x] Use pip freeze to update your requirements.txt file
* [x] Ran python manage.py test tests.test_feature_18 
* [x] and passed
* [x] Add, commit, and pushed changes

Feature 19
https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/85-assessment-project.md

* [x] In base.html in projects/templates:
* [x] a header tag as the first child of the body tag before the main tag that contains
  * [x] a nav tag that contains
    * [x] a ul tag that contains
      * [x] if the person is logged in,
        * [x] an li tag that contains
          * [x] an a tag with an href to the "show_my_tasks" path with the content "My tasks"
        * [x] an li tag that contains
          * [x] an a tag with an href to the "list_projects" path with the content "My projects"
        * [x] an li tag that contains
          * [x] an a tag with an href to the "logout" path with the content "Logout"
      * [x] otherwise
        * [x] an li tag that contains
          * [x] an a tag with an href to the "login" path with the content "Login"
        * [x] an li tag that contains
          * [x] an a tag with an href to the "signup" path with the content "Signup"
* [x] Use Block to extend to remaining HTML pages
* [x] ALL HTML PAGES, EVEN THE ONES IN ACCOUNTS APP
* [x] Ran python manage.py test tests.test_feature_18 
* [x] and passed
* [x] flake8 accounts projects tasks
* [x] djhtml -i «path to HTML template»
* [x] black --check accounts projects tasks
* [x] Add, commit, and pushed changes



