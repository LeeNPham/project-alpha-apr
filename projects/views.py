from django.shortcuts import render, redirect
from projects.forms import ProjectForm

from .models import Project
from django.contrib.auth.decorators import login_required


@login_required
def list_projects(request):
    user_projects = request.user.projects.all()
    context = {"projects": Project.objects.all, "user_projects": user_projects}
    return render(request, "projects/projects_list.html", context)


@login_required
def detail_project(request, pk):
    context = {
        "projects": Project.objects.get(pk=pk) if Project else None,
    }
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            return redirect("show_project", pk=project.pk)
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
