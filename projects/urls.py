from django.urls import path
from projects.views import create_project, detail_project, list_projects


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:pk>/", detail_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
