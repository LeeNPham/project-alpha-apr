from django.shortcuts import render, redirect

from tasks.models import Task
from .forms import CompletedTaskForm, TaskForm
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            project = form.save()
            return redirect("show_project", pk=project.pk)
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "projects/create_task.html", context)


@login_required
def task_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "projects/tasks_list.html", context)


def complete_task(request, pk):
    if Task and CompletedTaskForm:
        instance = Task.objects.get(pk=pk)
        if request.method == "POST":
            form = CompletedTaskForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("show_my_tasks")
        else:
            form = CompletedTaskForm(instance=instance)
    context = {
        "form": form,
    }
    return render(request, "projects/tasks_list.html", context)
