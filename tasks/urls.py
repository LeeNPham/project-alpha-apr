from django.urls import path
from tasks.views import complete_task, create_task, task_list

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", task_list, name="show_my_tasks"),
    path("<int:pk>/complete/", complete_task, name="complete_task"),
]
